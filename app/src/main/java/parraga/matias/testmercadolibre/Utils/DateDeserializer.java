package parraga.matias.testmercadolibre.Utils;

import android.util.Log;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String date = element.getAsString();
        if(date == null || date.isEmpty()){
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date result = null;
        try {
            result = format.parse(date);
        } catch (ParseException exp) {
            Log.e("DateDeserializer", exp.getMessage());
        } catch (Exception exp) {
            Log.e("DateDeserializer", exp.getMessage());
        }
        return result;
    }
}