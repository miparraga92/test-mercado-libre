package parraga.matias.testmercadolibre.Utils;

public class Evento {
    public String eventName;
    public Object data;

    public Evento(String eventName, Object data) {
        this.eventName = eventName;
        this.data=data;
    }

    public Evento(String eventName) {
        this.eventName = eventName;
    }
}
