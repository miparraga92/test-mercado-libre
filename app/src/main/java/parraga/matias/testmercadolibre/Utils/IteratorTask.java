package parraga.matias.testmercadolibre.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class IteratorTask<T> extends Thread {
    private Stack<T> listDown= new Stack<>();
    private AtomicInteger count;
    private int simultaneousWork = 2;
    private String eventNext;
    private String eventEnd;
    private String eventError;

    public IteratorTask(List<T> list){
        EventBus.getDefault().register(this);
        eventNext = UUID.randomUUID().toString();
        eventEnd = UUID.randomUUID().toString();
        eventError = UUID.randomUUID().toString();
        listDown.addAll(list);
    }

    @Override
    public void run(){
        init();
    }

    private void init(){
        if(listDown.size() == 0){
            EventBus.getDefault().post(new Evento(eventEnd));
            EventBus.getDefault().unregister(this);
            actionEnd();
            return;
        }
        count = new AtomicInteger(listDown.size());
        int i = 0;
        while (i < simultaneousWork && listDown.size() > 0){
            actionItem(listDown.pop());
            i++;
        }
    }

    public int getSimultaneousWork() {
        return simultaneousWork;
    }

    public void setSimultaneousWork(int simultaneousWork) {
        this.simultaneousWork = simultaneousWork;
    }

    public String getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(String eventEnd) {
        this.eventEnd = eventEnd;
    }

    public String getEventNext() {
        return eventNext;
    }

    public void setEventNext(String eventNext) {
        this.eventNext = eventNext;
    }

    public String getEventError() {
        return eventError;
    }

    public void setEventError(String eventError) {
        this.eventError = eventError;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEventListener(Evento event) {
        if(event.eventName.equals(eventNext) || event.eventName.equals(eventError)){
            if(listDown.size() != 0){
                actionItem(listDown.pop());
            }

            if(event.eventName.equals(eventNext)){
                actionBeforeNext(event.data);
            }

            if(event.eventName.equals(eventError)){
                actionError(event.data.toString());
            }

            if(count.decrementAndGet() == 0){
                EventBus.getDefault().post(new Evento(eventEnd));
                EventBus.getDefault().unregister(this);
                actionEnd();
            }
        }
    }

    public abstract void actionEnd();
    public abstract void actionBeforeNext(Object result);
    public abstract void actionError(String error);
    public abstract void actionItem(T item);


}