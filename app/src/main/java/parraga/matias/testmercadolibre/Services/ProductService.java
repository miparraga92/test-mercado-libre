package parraga.matias.testmercadolibre.Services;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import parraga.matias.testmercadolibre.Activities.AppController;
import parraga.matias.testmercadolibre.Configurations.Constant;
import parraga.matias.testmercadolibre.Models.Product;
import parraga.matias.testmercadolibre.Models.SearchResult;
import parraga.matias.testmercadolibre.R;
import parraga.matias.testmercadolibre.Utils.DateDeserializer;
import parraga.matias.testmercadolibre.Utils.Evento;

public class ProductService {

    public static final String TAG = ProductService.class.getSimpleName();

    public static void searchProducts(String txtSearch){
        AndroidNetworking.get(Constant.urlServer + "/sites/MLA/search?q=" + txtSearch)
                .addHeaders("Content-Type","application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getJSONObject("paging").getInt("total") > 0){
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
                                Gson gson = gsonBuilder.create();

                                ArrayList<SearchResult> items = new ArrayList<>();
                                JSONArray results = response.getJSONArray("results");
                                for (int i = 0; i < results.length(); i++) {
                                    SearchResult item = gson.fromJson(results.get(i).toString(), SearchResult.class);
                                    item.setFreeShipping(results.getJSONObject(i).getJSONObject("shipping").getBoolean("free_shipping"));
                                    item.setStateName(results.getJSONObject(i).getJSONObject("address").getString("state_name"));
                                    items.add(item);
                                }

                                EventBus.getDefault().post(new Evento("search_finish_success", items));
                            }
                        } catch (JSONException e) {
                            EventBus.getDefault().post(new Evento("search_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        } catch (Exception e) {
                            EventBus.getDefault().post(new Evento("product_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        EventBus.getDefault().post(new Evento("search_finish_error", AppController.getAppContext().getResources().getString(R.string.error_server)));
                        Log.e(TAG,anError.getErrorBody());
                    }
                });
    }

    public static void getById(String id){
        AndroidNetworking.get(Constant.urlServer + "/items/" + id)
                .addHeaders("Content-Type","application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
                                Gson gson = gsonBuilder.create();

                                Product item = gson.fromJson(response.toString(), Product.class);
                                item.setFreeShipping(response.getJSONObject("shipping").getBoolean("free_shipping"));

                                EventBus.getDefault().post(new Evento("product_finish_success", item));
                        } catch (JSONException e) {
                            EventBus.getDefault().post(new Evento("product_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        } catch (Exception e) {
                            EventBus.getDefault().post(new Evento("product_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        EventBus.getDefault().post(new Evento("product_finish_error", AppController.getAppContext().getResources().getString(R.string.error_server)));
                        Log.e(TAG,anError.getErrorBody());
                    }
                });
    }

    public static void getDescriptionById(String id){
        AndroidNetworking.get(Constant.urlServer + "/items/" + id + "/description")
                .addHeaders("Content-Type","application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String description = response.getString("plain_text");
                            EventBus.getDefault().post(new Evento("description_finish_success", description));
                        } catch (JSONException e) {
                            EventBus.getDefault().post(new Evento("description_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        } catch (Exception e) {
                            EventBus.getDefault().post(new Evento("product_finish_error", e.getMessage()));
                            Log.e(TAG, e.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        EventBus.getDefault().post(new Evento("description_finish_error", AppController.getAppContext().getResources().getString(R.string.error_server)));
                        Log.e(TAG,anError.getErrorBody());
                    }
                });
    }
}
