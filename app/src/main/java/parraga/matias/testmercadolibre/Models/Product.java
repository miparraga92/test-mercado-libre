package parraga.matias.testmercadolibre.Models;

import java.util.ArrayList;

public class Product {
    private String id;
    private String title;
    private String description;
    private double price;
    private String condition;
    private boolean freeShipping;
    private ArrayList<ProductPicture> pictures = new ArrayList<>();
    private ArrayList<ProductAttribute> attributes = new ArrayList<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<ProductPicture> getPictures() {
        return pictures;
    }

    public void setPictures(ArrayList<ProductPicture> pictures) {
        this.pictures = pictures;
    }

    public ArrayList<ProductAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<ProductAttribute> attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }
}
