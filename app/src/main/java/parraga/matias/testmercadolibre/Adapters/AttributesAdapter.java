package parraga.matias.testmercadolibre.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import parraga.matias.testmercadolibre.Models.ProductAttribute;
import parraga.matias.testmercadolibre.R;

public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.MyViewHolder> {

    private Context mContext;
    private List<ProductAttribute> productAttributes;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
        }
    }


    public AttributesAdapter(Context mContext, List<ProductAttribute> productAttributes) {
        this.mContext = mContext;
        this.productAttributes = productAttributes;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attribute_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ProductAttribute productAttribute = productAttributes.get(position);
        holder.title.setText(productAttribute.getName());
        holder.description.setText(productAttribute.getValue_name());
    }

    @Override
    public int getItemCount() {
        return productAttributes.size();
    }
}
