package parraga.matias.testmercadolibre.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.List;

import parraga.matias.testmercadolibre.Activities.ProductActivity;
import parraga.matias.testmercadolibre.Models.SearchResult;
import parraga.matias.testmercadolibre.R;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.MyViewHolder> {

    private Context mContext;
    private List<SearchResult> searchResults;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, price;
        public ImageView thumbnail, img_shipping;
        public View view;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            price = view.findViewById(R.id.price);
            thumbnail = view.findViewById(R.id.thumbnail);
            img_shipping = view.findViewById(R.id.img_shipping);
            this.view = view;
        }
    }


    public CardsAdapter(Context mContext, List<SearchResult> searchResults) {
        this.mContext = mContext;
        this.searchResults = searchResults;
    }

    public void clear(){
        this.searchResults.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SearchResult searchResult = searchResults.get(position);
        holder.title.setText(searchResult.getTitle());
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String moneyString = formatter.format(searchResult.getPrice()).replace(".00", "");
        holder.price.setText(moneyString);
        holder.price.setVisibility(View.VISIBLE);
        holder.description.setText(searchResult.getStateName());
        if (searchResult.isFreeShipping()){
            holder.img_shipping.setVisibility(View.VISIBLE);
        }else{
            holder.img_shipping.setVisibility(View.GONE);
        }

        if(searchResult.getThumbnail().isEmpty()){
            holder.thumbnail.setImageResource(R.drawable.default_image);
        }else{
            Glide.with(mContext)
                    .load(searchResult.getThumbnail())
                    .centerCrop()
                    .placeholder(R.drawable.loading_image)
                    .error(R.drawable.default_image)
                    .into(holder.thumbnail);
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click(searchResult);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(searchResult);
            }
        });
    }

    public void click(SearchResult searchResult){
        Intent intent = new Intent(mContext, ProductActivity.class);
        intent.putExtra("productId", searchResult.getId());
        intent.putExtra("stateName", searchResult.getStateName());
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }
}
