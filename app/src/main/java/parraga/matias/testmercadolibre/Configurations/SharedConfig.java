package parraga.matias.testmercadolibre.Configurations;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import parraga.matias.testmercadolibre.Activities.AppController;

public class SharedConfig {

    public static final String PREFS_FILENAME = "CONFIG_PREFS";
    public static final String PREFS_KEY_TOKEN = "CONFIG_PREFS_TOKEN";
    public static final String PREFS_TIENE_PLAYSERVICE = "CONFIG_PREFS_TIENE_PLAYSERVICE";

    public static void setTienePlayService(Context context, boolean tiene) {
        SharedPreferences settings;
        Editor editor;
        settings = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putBoolean(PREFS_TIENE_PLAYSERVICE, tiene); //3

        editor.commit(); //4
    }

    public static boolean getTienePlayService(Context context) {
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        return settings.getBoolean(PREFS_TIENE_PLAYSERVICE,false);
    }

    public static void saveToken(Context context, String token) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2

        editor.putString(PREFS_KEY_TOKEN, token); //3

        editor.commit(); //4
    }

    public static String getToken() {
        SharedPreferences settings;
        String token;
        settings = AppController.getAppContext().getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE);
        token = settings.getString(PREFS_KEY_TOKEN, null);
        return token;
    }
}
