package parraga.matias.testmercadolibre.Activities;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.androidnetworking.AndroidNetworking;

public class AppController extends MultiDexApplication {
    private  static  final String TAG = "AppController";
    private static Context context;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;

        context = getBaseContext();
        AndroidNetworking.initialize(getApplicationContext());
    }



    public static synchronized AppController getInstance() {
        if(mInstance == null)
        {
            mInstance = new AppController();
        }
        return mInstance;
    }

    public static Context getAppContext() {
        return context;
    }
}
