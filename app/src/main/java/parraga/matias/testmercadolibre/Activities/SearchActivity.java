package parraga.matias.testmercadolibre.Activities;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import parraga.matias.testmercadolibre.Adapters.CardsAdapter;
import parraga.matias.testmercadolibre.Models.SearchResult;
import parraga.matias.testmercadolibre.R;
import parraga.matias.testmercadolibre.Services.ProductService;
import parraga.matias.testmercadolibre.Utils.GridSpacingItemDecoration;
import parraga.matias.testmercadolibre.Utils.IteratorTask;
import parraga.matias.testmercadolibre.Utils.Util;

public class SearchActivity extends AppCompatActivity{

    public static final String TAG = SearchActivity.class.getSimpleName();

    private ArrayList<SearchResult> searchResults = new ArrayList<>();
    private RecyclerView recyclerView;
    private CardsAdapter adapter;
    private MaterialSearchView searchView;
    private ProgressDialog pDialog;
    private LinearLayout background;
    private int columns = 1;
    private TextView txtNotResult;
    private TextView txtTitle;
    private LinearLayout progressSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchView = findViewById(R.id.search_view);
        background = findViewById(R.id.background);
        txtNotResult = findViewById(R.id.txt_not_result);
        txtTitle = findViewById(R.id.txt_title);
        progressSearch = findViewById(R.id.progress_search);

        pDialog = new ProgressDialog(this);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);

        init();
        setEvents();

    }

    private int calcColumns(int width){
        int result = (int) (Util.convertPixelsToDp(width, SearchActivity.this) / 180);
        return result == 0 ? 1 : result;
    }

    private void init(){
        String stringSearch = getIntent().getStringExtra("stringSearch");
        if(!stringSearch.isEmpty()){
            search(stringSearch);
        }
    }

    private void setEvents(){
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String finalQuery = query.toLowerCase();
                search(finalQuery);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setVoiceSearch(false);

        background.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                int newColumns = calcColumns(v.getWidth());
                if(newColumns !=  columns){
                    columns = newColumns;
                    refreshResultView(searchResults);
                }
            }
        });
    }

    private void search(String stringSearch){
        if(!Util.isOnline()){
            showError("Verifique su conexión a internet.");
            return;
        }

        txtTitle.setText(stringSearch);
        txtNotResult.setVisibility(View.GONE);
        progressSearch.setVisibility(View.VISIBLE);

        if(adapter != null){
            ((CardsAdapter) recyclerView.getAdapter()).clear();
        }

        ArrayList<String> toSearch = new ArrayList<>();
        toSearch.add(stringSearch);
        IteratorTask iteratorTask = new IteratorTask<String>(toSearch) {
            @Override
            public void actionEnd() {
                SearchActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressSearch.setVisibility(View.GONE);
                        if(searchResults.size() == 0){
                            txtNotResult.setVisibility(View.VISIBLE);
                        }else{
                            txtNotResult.setVisibility(View.GONE);
                        }
                    }
                });
                refreshResultView(searchResults);
            }

            @Override
            public void actionBeforeNext(Object result) {
                searchResults = (ArrayList<SearchResult>) result;
            }

            @Override
            public void actionError(String error) {
                showError(error);
            }

            @Override
            public void actionItem(String item) {
                ProductService.searchProducts(item);
            }
        };

        iteratorTask.setEventNext("search_finish_success");
        iteratorTask.setEventError("search_finish_error");
        iteratorTask.run();
    }

    private void refreshResultView(ArrayList<SearchResult> searchResults){
        this.runOnUiThread(new Runnable() {
            public void run() {
                recyclerView = findViewById(R.id.recycler_view);
                adapter = new CardsAdapter(SearchActivity.this,  searchResults);

                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(SearchActivity.this, columns);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.addItemDecoration(new GridSpacingItemDecoration(columns, Util.dpToPx(2,SearchActivity.this), true));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }
        });
    }

    private void showError(String error){
        this.runOnUiThread(new Runnable() {
            public void run() {
                new MaterialDialog.Builder(SearchActivity.this)
                        .title("Aviso")
                        .content(error)
                        .positiveText(R.string.agree)
                        .show();
            }
        });
    }


    @Override
    protected void onResume(){
        super.onResume();
        if(pDialog != null && pDialog.isShowing()){
            pDialog.dismiss();
        }
    }

    @Override
    protected  void onPause(){
        super.onPause();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_search);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int spanCol = (int) width / 150;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCol, Util.dpToPx(10,this), true));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.nav_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.nav_search) {
//            searchView.openSearch();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
