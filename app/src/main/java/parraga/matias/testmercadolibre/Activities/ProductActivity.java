package parraga.matias.testmercadolibre.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import java.text.NumberFormat;
import java.util.ArrayList;

import parraga.matias.testmercadolibre.Adapters.AttributesAdapter;
import parraga.matias.testmercadolibre.Models.Product;
import parraga.matias.testmercadolibre.R;
import parraga.matias.testmercadolibre.Services.ProductService;
import parraga.matias.testmercadolibre.Utils.GridSpacingItemDecoration;
import parraga.matias.testmercadolibre.Utils.IteratorTask;
import parraga.matias.testmercadolibre.Utils.Util;

public class ProductActivity extends AppCompatActivity{

    public static final String TAG = ProductActivity.class.getSimpleName();

    private RecyclerView recyclerView;
    private AttributesAdapter adapter;
    private TextView txtDescription;
    private TextView txtPrice;
    private TextView txtTitle;
    private TextView txtStateName;
    private LinearLayout progressSearch;
    private CardView cardDescription;
    private CardView cardAttributes;
    private ScrollView productDetails;
    private LinearLayout llShipping;
    private CarouselView carouselView;

    private String stateName;
    private Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Producto");
        toolbar.setNavigationIcon(R.drawable.ic_arrow);
        toolbar.setNavigationOnClickListener(
                v -> onBackPressed()
        );

        txtPrice = findViewById(R.id.txt_price);
        txtDescription = findViewById(R.id.txt_description);
        txtTitle = findViewById(R.id.txt_title);
        txtStateName = findViewById(R.id.txt_state_name);
        progressSearch = findViewById(R.id.progress_search);
        recyclerView = findViewById(R.id.attributes_list);
        cardDescription = findViewById(R.id.card_description);
        cardAttributes = findViewById(R.id.card_attributes);
        productDetails = findViewById(R.id.product_details);
        llShipping = findViewById(R.id.ll_shipping);
        carouselView = findViewById(R.id.carouselView);

        init();
        setEvents();
    }

    private void init(){
        String productId = getIntent().getStringExtra("productId");
        stateName = getIntent().getStringExtra("stateName");
        if(!productId.isEmpty()){
            getProduct(productId);
        }
    }

    private void setEvents(){

    }

    private void getProduct(String id){
        ArrayList<String> toSearch = new ArrayList<>();
        toSearch.add(id);
        IteratorTask iteratorTask = new IteratorTask<String>(toSearch) {
            @Override
            public void actionEnd() {
                getDescriptionProduct(id);
            }

            @Override
            public void actionBeforeNext(Object result) {
                product = (Product) result;
            }

            @Override
            public void actionError(String error) {
                showError(error);
            }

            @Override
            public void actionItem(String item) {
                ProductService.getById(item);
            }
        };

        iteratorTask.setEventNext("product_finish_success");
        iteratorTask.setEventError("product_finish_error");
        iteratorTask.run();
    }

    private void getDescriptionProduct(String id){
        ArrayList<String> toSearch = new ArrayList<>();
        toSearch.add(id);
        IteratorTask iteratorTask = new IteratorTask<String>(toSearch) {
            @Override
            public void actionEnd() {
                ProductActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        setFields();
                        setAttributes();
                        progressSearch.setVisibility(View.GONE);
                        productDetails.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void actionBeforeNext(Object result) {
                product.setDescription(result.toString());
            }

            @Override
            public void actionError(String error) {
                showError(error);
            }

            @Override
            public void actionItem(String item) {
                ProductService.getDescriptionById(item);
            }
        };

        iteratorTask.setEventNext("description_finish_success");
        iteratorTask.setEventError("description_finish_error");
        iteratorTask.run();
    }

    private void setFields(){
        this.txtTitle.setText(product.getTitle());
        this.txtStateName.setText(stateName);
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String moneyString = formatter.format(product.getPrice()).replace(".00", "");
        this.txtPrice.setText(moneyString);

        if(product.getDescription() != null && !product.getDescription().isEmpty()){
            txtDescription.setText(product.getDescription());
            cardDescription.setVisibility(View.VISIBLE);
        }

        if(product.isFreeShipping()){
            this.llShipping.setVisibility(View.VISIBLE);
        }

        int pageCount = (product.getPictures() == null || product.getPictures().size() == 0)? 1 : product.getPictures().size();
        carouselView.setViewListener(viewListener);
        carouselView.setPageCount(pageCount);
    }

    private void setAttributes(){
        if(product != null && product.getAttributes().size() == 0){
            return;
        }

        cardAttributes.setVisibility(View.VISIBLE);
        adapter = new AttributesAdapter(ProductActivity.this,  product.getAttributes());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ProductActivity.this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, Util.dpToPx(2,ProductActivity.this), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void showError(String error){
        this.runOnUiThread(new Runnable() {
            public void run() {
                new MaterialDialog.Builder(ProductActivity.this)
                        .title("Aviso")
                        .content(error)
                        .positiveText(R.string.agree)
                        .show();
            }
        });
    }

    private ViewListener viewListener = new ViewListener() {

        @Override
        public View setViewForPosition(int position) {
            View customView = getLayoutInflater().inflate(R.layout.carousel_view, null);
            ImageView imageView = customView.findViewById(R.id.carousel_image);
            if(product.getPictures() != null && product.getPictures().size() > 0){
                String url = product.getPictures().get(position).getUrl();
                String finalUrl = url;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goFullScreenImage(finalUrl);
                    }
                });
                Glide
                    .with(ProductActivity.this)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.loading_image)
                    .error(R.drawable.default_image)
                    .into(imageView);
            }
            return customView;
        }
    };

    private void goFullScreenImage(String url){
        Intent intent = new Intent(ProductActivity.this, FullScreenImageActivity.class);
        intent.putExtra("path", url);
        ProductActivity.this.startActivity(intent);
    }
}
