package parraga.matias.testmercadolibre.Activities;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import parraga.matias.testmercadolibre.R;

public class HomeActivity extends AppCompatActivity {

    public static final String TAG = HomeActivity.class.getSimpleName();

    private EditText txtSearch;
    private TextView txtSlogan;
    private ImageButton btnSearch;
    private ImageView imgBackground;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        txtSearch = findViewById(R.id.txt_search);
        btnSearch = findViewById(R.id.btn_search);
        imgBackground = findViewById(R.id.img_background);
        txtSlogan = findViewById(R.id.txt_slogan);
        mp = MediaPlayer.create(this, R.raw.keys);

        setEvents();
        initAnimations();
    }

    private void setEvents(){
        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });
    }

    private void initAnimations(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.zoom_anim);
        animation.setRepeatCount(Animation.INFINITE);
        imgBackground.startAnimation(animation);
        new Thread(){
            public void run(){
                try {
                    Thread.sleep(800);

                    String txtFinal = "¿Que estas buscando?";
                    String txtPartial="";
                    int i=0;
                    mp.start();
                    while (!txtFinal.equals(txtPartial)){
                        Thread.sleep(40);
                        txtPartial += txtFinal.charAt(i);
                        String finalTxtPartial1 = txtPartial;
                        runOnUiThread (new Thread(new Runnable() {
                            public void run() {
                                txtSlogan.setText(finalTxtPartial1);
                            }
                        }));
                        i++;
                    }
                    while (true){
                        Thread.sleep(700);
                        if(txtPartial.endsWith("_")){
                            txtPartial = txtPartial.replace("_", "");
                        }else {
                            txtPartial += "_";
                        }
                        String finalTxtPartial = txtPartial;
                        runOnUiThread (new Thread(new Runnable() {
                            public void run() {
                                txtSlogan.setText(finalTxtPartial);
                            }
                        }));
                    }
                } catch (InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }.start();
    }

    private void search(){
        String stringSearch = txtSearch.getText().toString();
        if(!stringSearch.isEmpty()){
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra("stringSearch", stringSearch);
            startActivity(intent);
            finish();
        }
    }
}
